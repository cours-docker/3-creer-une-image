<?php

require_once __DIR__ . '/config/bootstrap.php';

// Nombre de visites totales sur le site
$visit_count = $con->query('SELECT COUNT(*) FROM visit')->fetchColumn();

// 10 dernière visites
$last_visits = $con->query('SELECT * FROM visit ORDER BY visited_at DESC LIMIT 10');
$last_visits = $last_visits->fetchAll(PDO::FETCH_ASSOC);

?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <title>Accueil</title>
</head>
<body>
    <?php include __DIR__ . '/includes/header.php'; ?>

    <div class="container border mt-4 p-4">
        <h1>Accueil</h1>

        <p>
            Nombre de visites sur le site: <strong><?= $visit_count; ?></strong><br>
            Version de l'application: <strong>v<?= $_ENV['APP_VERSION']; ?></strong>
        </p>

        <h3>10 dernières visites:</h3>
        <table class="table">
            <tr>
                <th>#</th>
                <th>Page visitée</th>
                <th>Heure de la visite</th>
            </tr>

            <?php foreach($last_visits as $visit): ?>
                <tr>
                    <th><?= $visit['id']; ?></th>
                    <th><?= $visit['path']; ?></th>
                    <th><?= (new DateTime($visit['visited_at']))->format('d/m/Y H:i'); ?></th>
                </tr>
            <?php endforeach; ?>
        </table>
    </div>

    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
</body>
</html>
