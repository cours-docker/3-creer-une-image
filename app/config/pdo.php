<?php

// DSN pour PDO
$dsn = sprintf(
    'mysql:host=%s;port=%s;dbname=%s;',
    $_ENV['DB_HOST'],
    $_ENV['DB_PORT'],
    $_ENV['DB_NAME']
);

// Connexion PDO
try {
    $con = new PDO($dsn, $_ENV['DB_USER'], $_ENV['DB_PASS']);
} catch (Exception $e) {
    die('ERREUR PDO:' . $e->getMessage());
}