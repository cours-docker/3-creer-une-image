<?php

// Création de la table "visit"
$con->exec(
    'CREATE TABLE IF NOT EXISTS visit (
        id INT NOT NULL AUTO_INCREMENT,
        path VARCHAR(255) NOT NULL,
        visited_at DATETIME NOT NULL, 
        PRIMARY KEY (id)
    )ENGINE=INNODB;'
);